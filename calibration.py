import serial
import time
from time import sleep
import datetime
from pathlib import Path
import http.server
from http.server import HTTPServer
import threading
import socket

# initialize sensors----------------------------------------------------------------------
ser = []
port = 0
portcheck = False
while(port < 99):
    addr = '/dev/ttyUSB' + str(port)
    serfile = Path(addr)
    if serfile.exists():
        print(addr + " is present.")
        portcheck = False
        try:
            ser.append(serial.Serial(addr, baudrate=9600, parity=serial.PARITY_NONE, bytesize=serial.EIGHTBITS, stopbits=serial.STOPBITS_ONE))
        except OSError as e:
            print (addr + " is INVALID")
        else:
            print(addr + " is valid")
    else:
        if (portcheck == False):
            print(addr + " is NOT present.")
        portcheck = True
    port = port + 1

def sendreadcode(serx):
    serx.write(b'R\r')

def sendidcode(serx):
    ser[x].write(b'i\r')

def sendtemptrain(serx, temp):
    serx.write(b'T,' + str.encode(str(temp)) + b'\r')

def checkok(serx):
    message = ""
    raw = ""
    while raw != b'\r':
        raw = serx.read()
        message += raw.decode('utf-8')
    if ("OK" in message):
       return True
    else:
        print ("CHECKOK FAILED ", message)
        return False

def getidreading(serx):
    message = ""
    raw = ""
    while raw != b',':
        raw = serx.read()
    raw = ""
    while raw != b'\r':
        raw = serx.read()
        message += raw.decode('utf-8')
    message = message.rstrip(',')
    return message

def getfirmwarereading(serx):
    message = ""
    raw = ""
    while raw != b',':
        raw = serx.read()
        raw = ""
    while raw != b',':
        raw = serx.read()
        raw=""
    while raw != b'\r':
        raw = serx.read()
        print(raw)
        message += raw.decode('utf-8')
    return message

def getreading(serx):
    message = ""
    raw = ""
    while raw != b'\r':
        raw = serx.read()
        message += raw.decode('utf-8')
    if ("OK" in message):
        message = ""
        raw = ""
        while raw != b'\r':
            raw = serx.read()
            message += raw.decode('utf-8')
    return message

def checkcal(serx):
    message=""
    serx.write(b'Cal,?\r')
    while("CAL" not in message):
        message = getreading(serx)
    print(message)
    if ("CAL" in message):
        if("0" in message):
            return("This sensor is uncalibrated.")
        if ("1" in message):
            return ("This sensor is Calibrated. 1 point.")
        if ("2" in message):
            return ("This sensor is Calibrated. 2 point.")
        if ("3" in message):
            return ("This sensor is Calibrated. 3 point.")

def sendcalclearcode(serx):
    serx.write(b'Cal,clear\r')

#Tell sensors to shut up------------------------------------------------------------------
for x in range(len(ser)):
    ser[x].write(b'C,0\r')
    sleep(1)

sensorTypes = []
sensorFirm = []
#Retrieve Sensor Types---------------------------------------------------------------------
for x in range(len(ser)):
    sendidcode(ser[x])
    print("Querying Sensor ",  x)
    idreading = ((getidreading(ser[x]).split(",")))
    sensorTypes.append(idreading[0])
    sensorFirm.append(idreading[1])
while True:
    print()
    for x in range(len(ser)):
        print(x, sensorTypes[x], sensorFirm[x])
    sensorChoice = int(input("Choose a sensor number to calibrate: "))
    optionChoice = int(input("1-Inquiry, 2-Clear, 3-Calibrate: "))
    if (optionChoice == 1):
        print(checkcal(ser[sensorChoice]))
        sleep(2)
    if (optionChoice == 2):
        sendcalclearcode(ser[sensorChoice])
        print("Decalibration sent.")
        print(checkcal(ser[sensorChoice]))
        sleep(2)
    if (optionChoice == 3):
        if (sensorTypes[sensorChoice] == "RTD"):
            temp = round(float(input("Input calibration temp in C: ")),2)
            ser[sensorChoice].write(b'Cal,' + str.encode(str(temp)) + b'\r')
            print("Calibration sent.")
            sleep(2)
            print(checkcal(ser[sensorChoice]))
            sleep(2)
        if (sensorTypes[sensorChoice] == "pH"):
            print("Choose 1, 2, or 3 point calibration.")
            phchoice = int(input("1, 2 or 3:  "))
            if (phchoice == 1):
                ph = round(float(input("Input calibration PH: ")), 2)
                ser[sensorChoice].write(b'Cal,mid,' + str.encode(str(ph)) + b'\r')
                print("Calibration Sent.")
                sleep(2)


            if (phchoice == 2):
                ph = round(float(input("Input HIGH calibration PH: ")), 2)
                ser[sensorChoice].write(b'Cal,mid,' + str.encode(str(ph)) + b'\r')
                print("Calibration Sent.")
                sleep(2)


                ph = round(float(input("Input LOW calibration PH: ")), 2)
                ser[sensorChoice].write(b'Cal,low,' + str.encode(str(ph)) + b'\r')
                print("Calibration Sent.")
                sleep(2)


            if (phchoice == 3):
                ph = round(float(input("Input MID calibration PH: ")), 2)
                ser[sensorChoice].write(b'Cal,mid,' + str.encode(str(ph)) + b'\r')
                print("Calibration Sent.")
                sleep(2)


                ph = round(float(input("Input LOW calibration PH: ")), 2)
                ser[sensorChoice].write(b'Cal,low,' + str.encode(str(ph)) + b'\r')
                print("Calibration Sent.")
                sleep(2)

                ph = round(float(input("Input HIGH calibration PH: ")), 2)
                ser[sensorChoice].write(b'Cal,high,' + str.encode(str(ph)) + b'\r')
                print("Calibration Sent.")
                sleep(2)
            print(checkcal(ser[sensorChoice]))
            sleep(2)
        if (sensorTypes[sensorChoice] == "EC"):
            print("Choose 1, or 2 point calibration.")
            ecchoice = int(input("1 or 2:  "))
            if (ecchoice == 1):
                inec = input("DRY calibration. Press enter when ready.")
                ser[sensorChoice].write(b'Cal,dry\r')
                print("Calibration sent.")
                sleep(2)

                ec = int(input("Input calibration TDS: "))
                ser[sensorChoice].write(b'Cal,' + str.encode(str(ec)) + b'\r')
                print("Calibration sent.")
                sleep(2)

            if (ecchoice == 2):
                inec = input("DRY calibration. Press enter when ready.")
                ser[sensorChoice].write(b'Cal,dry\r')
                print("Calibration sent.")
                sleep(2)

                ec = int(input("Input LOW calibration TDS: "))
                ser[sensorChoice].write(b'Cal,low,' + str.encode(str(ec)) + b'\r')
                print("Calibration sent.")
                sleep(2)

                ec = int(input("Input HIGH calibration TDS: "))
                ser[sensorChoice].write(b'Cal,high' + str.encode(str(ec)) + b'\r')
                print("Calibration sent.")
                sleep(2)
            print(checkcal(ser[sensorChoice]))
            sleep(2)
