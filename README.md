# ALULASense
A python program to read, log, and display values from any number of connected ATLAS Scientific USB Carrier Boards

index.html - an example file. Hosted by the python webserver, this will be overwritten repeatedly.

calibration.py - Program used to calibrate circuits. Currently supports RTD, pH and EC.

continuous.py - Continuous logging and web interface.

calibrate.sh & continuous.sh - shortcut executables used to launch the .py files in terminal.  Modify to replace terminal command and file location.
