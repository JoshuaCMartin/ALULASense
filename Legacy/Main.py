import serial
from time import sleep
import datetime
from pathlib import Path
# initialize sensors

ser = []
port = 0
portcheck = False
while(len(ser) < 3):
    addr = '/dev/ttyUSB' + str(port)
    serfile = Path(addr)
    if serfile.exists():
        print(addr + " is present.")
        portcheck = False
        try:
            ser.append(serial.Serial(addr, baudrate=9600, parity=serial.PARITY_NONE, bytesize=serial.EIGHTBITS, stopbits=serial.STOPBITS_ONE))
        except OSError as e:
            print (addr + " is INVALID")
        else:
            print(addr + " is valid")
    else:
        if (portcheck == False):
            print(addr + " is NOT present.")
        portcheck = True
    port = port + 1
    if (port > 99):
        print("All sensors NOT present & functioning.")
        input("Press ENTER to quit.")
        exit()
print("All sensors present & functioning.")
numReadings = 5
sensorTypes = ["", "", ""]


def clrmsg():
    global message
    message=""
    global raw
    raw=""


def sendreadcode(serx):
    serx.write(b'R\r')

def sendidcode(serx):
    ser[x].write(b'i\r')

def sendtemptrain(serx, temp):
    serx.write(b'T,' + str.encode(str(temp)) + b'\r')

def checkok(serx):
    message = ""
    raw = ""
    while raw != b'\r':
        raw = serx.read()
        message += raw.decode('utf-8')
    if ("OK" in message):
       return True
    else:
        print ("CHECKOK FAILED ", message)
        return False

def getidreading(serx):
    message = ""
    raw = ""
    while raw != b',':
        raw = serx.read()
    raw = ""
    while raw != b',':
        raw = serx.read()
        message += raw.decode('utf-8')
    message = message.rstrip(',')
    return message

def getreading(serx):
    message = ""
    raw = ""
    while raw != b'\r':
        raw = serx.read()
        message += raw.decode('utf-8')
    if ("OK" in message):
        message = ""
        raw = ""
        while raw != b'\r':
            raw = serx.read()
            message += raw.decode('utf-8')
    return message





for x in range(len(ser)):
    ser[x].write(b'C,0\r')
    sleep(1)
    #checkok(ser[x])


#Retrieve Sensor Types
for x in range(len(ser)):
    clrmsg()
    sendidcode(ser[x])
    print("Querying Sensor ",  x )
    sensorTypes[x] = getidreading(ser[x])



print(sensorTypes)
print("all done")


rtdIndex = sensorTypes.index("RTD")
phIndex = sensorTypes.index("pH")
ecIndex = sensorTypes.index("EC")

#PH and EC inexplicably send readings with no prompting.
getreading(ser[phIndex])
getreading(ser[ecIndex])
checkok(ser[phIndex])
checkok(ser[ecIndex])
#first temp reading is trash
sendreadcode(ser[rtdIndex])
getreading(ser[rtdIndex])
checkok(ser[rtdIndex])


#get initial temp data for calibration


rtdReadings = []
phReadings = []
ecReadings = []
tdsReadings = []
rtdAvg = 0.00
phAvg = 0.00
ecAvg = 0.00
tdsAvg = 0.00

for x in range(numReadings):
    rtdReadings.append(0)
    phReadings.append(0)
    ecReadings.append(0)
    tdsReadings.append(0)

for x in range(numReadings):
    clrmsg()
    sendreadcode(ser[rtdIndex])
    rtdReadings[x]=float(getreading(ser[rtdIndex]).rstrip('\r'))
    checkok(ser[rtdIndex])
    sleep (1)
print("initial temps ", rtdReadings)
rtdAvg = round(sum(rtdReadings) / len(rtdReadings),1)

#Main Loop
print("Init Avg Temp = ", rtdAvg, "c")
while True:
    #Train PH Meter
    sendtemptrain(ser[phIndex], rtdAvg)

    #Train EC Meter
    sendtemptrain(ser[ecIndex], rtdAvg)
    checkok(ser[ecIndex])

    for x in range(numReadings):
        #get temps
        sendreadcode(ser[rtdIndex])
        rtdReadings[x]=float(getreading(ser[rtdIndex]).rstrip('\r'))
        checkok(ser[rtdIndex])

        #get ec & tds readings
        sendreadcode(ser[ecIndex])

        msgx = getreading(ser[ecIndex]).split(",")
        tdsReadings[x]=float(msgx[0])
        ecReadings[x]=float(msgx[1].rstrip('\r'))
        checkok(ser[ecIndex])

        #get PH readings
        sendreadcode(ser[phIndex])
        phReadings[x]=float(getreading(ser[phIndex]).rstrip('\r'))
        checkok(ser[phIndex])
    #print("Temps ",rtdReadings)
    #print("PHs ", phReadings)
    #print("ECs ", ecReadings)
    #print("TDS", tdsReadings)
    rtdAvg = round(sum(rtdReadings) / len(rtdReadings), 1)
    phAvg = round(sum(phReadings) / len(phReadings), 2)
    ecAvg = round(sum(ecReadings) / len(ecReadings), 2)
    tdsAvg = round(sum(tdsReadings) / len(tdsReadings), 2)
    rtdAvgF = round((rtdAvg*9/5)+32, 2)
    print()
    print(datetime.datetime.now())
    print("Num Readings = ", numReadings)
    print("Temp is ", rtdAvg, "c or ", rtdAvgF, "F")
    print("PH   is ", phAvg)
    print("EC   is ", ecAvg)
    print("TDS  is ", tdsAvg)




