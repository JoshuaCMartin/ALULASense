import serial
from time import sleep
import datetime
from pathlib import Path

# initialize sensors
ser = []
port = 0
portcheck = False
while(len(ser) < 3):
    addr = '/dev/ttyUSB' + str(port)
    serfile = Path(addr)
    if serfile.exists():
        print(addr + " is present.")
        portcheck = False
        try:
            ser.append(serial.Serial(addr, baudrate=9600, parity=serial.PARITY_NONE, bytesize=serial.EIGHTBITS, stopbits=serial.STOPBITS_ONE))
        except OSError as e:
            print (addr + " is INVALID")
        else:
            print(addr + " is valid")
    else:
        if (portcheck == False):
            print(addr + " is NOT present.")
        portcheck = True
    port = port + 1
    if (port > 99):
        print("All sensors NOT present & functioning.")
        input("Press ENTER to quit.")
        exit()
print("All sensors present & functioning.")
sensorTypes = ["", "", ""]

def clrmsg():
    global message
    message=""
    global raw
    raw=""


def sendreadcode(serx):
    serx.write(b'R\r')

def sendidcode(serx):
    ser[x].write(b'i\r')

def sendtemptrain(serx, temp):
    temp = round(temp, 2)
    serx.write(b'T,' + str.encode(str(temp)) + b'\r')

def checkok(serx):
    message = ""
    while("OK" not in message):
        message = ""
        raw = ""
        while raw != b'\r':
            raw = serx.read()
            message += raw.decode('utf-8')
        if ("ER" in message):
            print("Checkok failed: " + message)
            return False
    return True


def getidreading(serx):
    message = ""
    raw = ""
    while raw != b',':
        raw = serx.read()
    raw = ""
    while raw != b',':
        raw = serx.read()
        message += raw.decode('utf-8')
    message = message.rstrip(',')
    return message

def getreading(serx):
    message = ""
    raw = ""
    while raw != b'\r':
        raw = serx.read()
        message += raw.decode('utf-8')
    while ("OK" in message):
        message = ""
        raw = ""
        while raw != b'\r':
            raw = serx.read()
            message += raw.decode('utf-8')
    return message





for x in range(len(ser)):
    ser[x].write(b'C,0\r')
    sleep(1)
    checkok(ser[x])


#Retrieve Sensor Types
for x in range(len(ser)):
    clrmsg()
    sendidcode(ser[x])
    print("Querying Sensor ",  x )
    sensorTypes[x] = getidreading(ser[x])



print(sensorTypes)
print("all done")


rtdIndex = sensorTypes.index("RTD")
phIndex = sensorTypes.index("pH")
ecIndex = sensorTypes.index("EC")
tdsIndex = ecIndex

#PH and EC inexplicably send readings with no prompting.
getreading(ser[phIndex])
getreading(ser[ecIndex])
checkok(ser[phIndex])
checkok(ser[ecIndex])
#first temp reading is trash
sendreadcode(ser[rtdIndex])
getreading(ser[rtdIndex])
checkok(ser[rtdIndex])






def getrtdreading():
    message = ""
    sendreadcode(ser[rtdIndex])
    message = round(float(getreading(ser[rtdIndex]).rstrip('\r')), 2)
    checkok(ser[rtdIndex])
    return message

def gettempf(temp):
    tempf = round(((temp * 9 / 5)+32), 2)
    return tempf

def getphreading():
    temp=getrtdreading()
    sendtemptrain(ser[phIndex], temp)
    message = ""
    sendreadcode(ser[phIndex])
    message = round(float(getreading(ser[phIndex]).rstrip('\r')), 2)
    checkok(ser[phIndex])
    return message

def getecreading():
    temp = getrtdreading()
    sendtemptrain(ser[ecIndex], temp)
    sendreadcode(ser[ecIndex])
    msgx = getreading(ser[ecIndex]).split(",")
    message = round(float(msgx[1].rstrip('\r')), 2)
    checkok(ser[ecIndex])
    return message

def gettdsreading():
    temp = getrtdreading()
    sendtemptrain(ser[tdsIndex], temp)
    sendreadcode(ser[tdsIndex])
    msgx = getreading(ser[tdsIndex]).split(",")
    message = round(float(msgx[0].rstrip('\r')), 2)
    checkok(ser[tdsIndex])
    return message


def sendcaltempcode(temp):
    temp = round(temp, 2)
    ser[rtdIndex].write(b'Cal,' + str.encode(str(temp)) + b'\r')
    if(checkok(ser[rtdIndex]) == True):
        print("Calibration successful.")


def sendcalclearcode(serx):
    serx.write(b'Cal,clear\r')



def checkcal(serx):
    message=""
    serx.write(b'Cal,?\r')
    while("CAL" not in message):
        message = getreading(serx)
    print(message)
    if ("CAL" in message):
        if("0" in message):
            return("This sensor is uncalibrated.")
        if ("1" in message):
            return ("This sensor is Calibrated. 1 point.")
        if ("2" in message):
            return ("This sensor is Calibrated. 2 point.")
        if ("3" in message):
            return ("This sensor is Calibrated. 3 point.")




while(True):
    print()
    print("Choose a sensor for readings:")
    print("5 for Calibration")
    x = input("0-All, 1-Temp, 2-PH, 3-EC, 4-TDS:  ")
    in1 = int(x)
    #if (in1 == 1 or in1 == 2 or in1 == 3):
    #    x = input("Temperature Compensation? 0=No, 1=Yes:  ")
    #    in2 = int(x)
    #x = input("1-Average, 2-Multi:  ")
    #in3 = int(x)
    if (in1 != 5):
        x = input("Enter Number of Readings:  ")
        in4 = int(x)

    #temp option
    if (in1 == 1):
        tempavg = 0
        for q in range(in4):
            temp = getrtdreading()
            tempf = gettempf(temp)
            print("Temp ", q+1, "is:  ", temp, "c or ", tempf, "F")
            tempavg = tempavg + temp
            sleep(1)
        tempavg = round((tempavg / in4), 2)
        tempfavg = gettempf(tempavg)
        print("Avg Temp:  ", tempavg, "c or ", tempfavg, "F")
        sleep(2)

    # ph option
    if (in1 == 2):
        phavg = 0
        for q in range(in4):
            ph = getphreading()
            print("PH ", q + 1, "is:  ", ph)
            phavg = phavg + ph
            sleep(1)
        phavg = round((phavg / in4), 2)
        print("Avg PH:  ", phavg)
        sleep(2)

    # ec option
    if (in1 == 3):
        ecavg = 0
        for q in range(in4):
            ec = getecreading()
            print("EC ", q + 1, "is:  ", ec)
            ecavg = ecavg + ec
            sleep(1)
        ecavg = round((ecavg / in4), 2)
        print("Avg EC:  ", ecavg)
        sleep(2)

    # tds option
    if (in1 == 4):
        tdsavg = 0
        for q in range(in4):
            tds = gettdsreading()
            print("TDS ", q + 1, "is:  ", tds)
            tdsavg = tdsavg + tds
            sleep(1)
        tdsavg = round((tdsavg / in4), 2)
        print("Avg TDS:  ", tdsavg)
        sleep(2)

    if (in1 == 0):
        tempavg = 0
        phavg = 0
        ecavg = 0
        tdsavg = 0
        for q in range(in4):
            print()
            print(datetime.datetime.now())
            temp = getrtdreading()
            tempf = gettempf(temp)
            tempavg = tempavg + temp
            ph = getphreading()
            phavg = phavg + ph
            ec = getecreading()
            ecavg = ecavg + ec
            tds = gettdsreading()
            tdsavg = tdsavg + tds
            print("Temp ", q + 1, "is:  ", temp, "c or ", tempf, "F")
            print("PH ", q + 1, "is:  ", ph)
            print("EC ", q + 1, "is:  ", ec)
            print("TDS ", q + 1, "is:  ", tds)
            sleep(1)

        tempavg = round((tempavg / in4), 2)
        tempfavg = gettempf(tempavg)
        print()
        print(datetime.datetime.now())
        print("Avg temp:  ", tempavg, "c or ", tempfavg, "F")

        phavg = round((phavg / in4), 2)
        print("Avg PH:  ", phavg)

        ecavg = round((ecavg / in4), 2)
        print("Avg EC:  ", ecavg)

        tdsavg = round((tdsavg / in4), 2)
        print("Avg TDS:  ", tdsavg)
        sleep(2)
    if (in1 == 5):
        while(True):
            print()
            print("Calibration mocde:")
            print("Choose a sensor. 0 to exit.")
            in5 = input("1-Temp, 2-PH, 3-EC-TDS: ")
            senschoice = int(in5)
            if (senschoice == 0):
                break
            print("Choose an option.")
            in6 = input("1-Inquiry, 2-Clear, 3-Calibrate:  ")
            optchoice = int(in6)

            if (optchoice == 1):
                if (senschoice == 1):
                    print(checkcal(ser[rtdIndex]))
                    sleep(2)
                if (senschoice == 2):
                    print(checkcal(ser[phIndex]))
                    sleep(2)
                if (senschoice == 3):
                    print(checkcal(ser[ecIndex]))
                    sleep(2)

            if (optchoice == 2):
                if (senschoice == 1):
                    sendcalclearcode(ser[rtdIndex])
                    print("Decalibration successful.")
                    sleep(2)

                if (senschoice == 2):
                    sendcalclearcode(ser[phIndex])
                    print("Decalibration successful.")
                    sleep(2)

                if (senschoice == 3):
                    sendcalclearcode(ser[ecIndex])
                    print("Decalibration successful.")
                    sleep(2)

            if (optchoice == 3):

                if (senschoice == 1):
                    intemp = input ("Input calibration temp in C: ")

                    temp = round(float(intemp), 2)
                    ser[rtdIndex].write(b'Cal,' + str.encode(str(temp)) + b'\r')
                    if (checkok(ser[rtdIndex]) == True):
                        print("Calibration successful.")
                        sleep(2)

                if (senschoice == 2):
                    print("Choose 1, 2, or 3 point calibration.")
                    inphcal = input("1, 2 or 3:  ")
                    phchoice = int(inphcal)
                    if (phchoice == 1):
                        inph = input("Input calibration PH: ")
                        ph = round(float(inph), 2)
                        ser[phIndex].write(b'Cal,mid,' + str.encode(str(ph)) + b'\r')
                        if (checkok(ser[phIndex]) == True):
                            print("Calibration successful.")
                            sleep(2)

                    if (phchoice == 2):
                        inph = input("Input HIGH calibration PH: ")
                        ph = round(float(inph), 2)
                        ser[phIndex].write(b'Cal,mid,' + str.encode(str(ph)) + b'\r')
                        if (checkok(ser[phIndex]) == True):
                            print("Calibration successful.")
                            sleep(2)

                        inph = input("Input LOW calibration PH: ")
                        ph = round(float(inph), 2)
                        ser[phIndex].write(b'Cal,low,' + str.encode(str(ph)) + b'\r')
                        if (checkok(ser[phIndex]) == True):
                            print("Calibration successful.")
                            sleep(2)


                    if (phchoice == 3):
                        inph = input("Input MID calibration PH: ")
                        ph = round(float(inph), 2)
                        ser[phIndex].write(b'Cal,mid,' + str.encode(str(ph)) + b'\r')
                        if (checkok(ser[phIndex]) == True):
                            print("Calibration successful.")
                            sleep(2)

                        inph = input("Input LOW calibration PH: ")
                        ph = round(float(inph), 2)
                        ser[phIndex].write(b'Cal,low,' + str.encode(str(ph)) + b'\r')
                        if (checkok(ser[phIndex]) == True):
                            print("Calibration successful.")
                            sleep(2)

                        inph = input("Input HIGH calibration PH: ")
                        ph = round(float(inph), 2)
                        ser[phIndex].write(b'Cal,high,' + str.encode(str(ph)) + b'\r')
                        if (checkok(ser[phIndex]) == True):
                            print("Calibration successful.")
                            sleep(2)

                if (senschoice == 3):
                    print("Choose 2, or 3 point calibration.")
                    ineccal = input("2 or 3:  ")
                    ecchoice = int(ineccal)

                    if (ecchoice == 2):
                        inec = input("DRY calibration. Press enter when ready.")
                        ser[ecIndex].write(b'Cal,dry\r')
                        if (checkok(ser[ecIndex]) == True):
                            print("Calibration successful.")
                            sleep(2)

                        inec = input("Input calibration TDS: ")
                        ec = int(inec)
                        print(b'Cal,' + str.encode(str(ec)) + b'\r')
                        ser[ecIndex].write(b'Cal,' + str.encode(str(ec)) + b'\r')
                        if (checkok(ser[ecIndex]) == True):
                            print("Calibration successful.")
                            sleep(2)


                    if (ecchoice == 3):
                        inec = input("DRY calibration. Press enter when ready.")
                        ser[ecIndex].write(b'Cal,dry\r')
                        if (checkok(ser[ecIndex]) == True):
                            print("Calibration successful.")
                            sleep(2)

                        inec = input("Input LOW calibration TDS: ")
                        ec = int(inec)
                        ser[ecIndex].write(b'Cal,low,' + str.encode(str(ec)) + b'\r')
                        if (checkok(ser[ecIndex]) == True):
                            print("Calibration successful.")
                            sleep(2)

                        inec = input("Input HIGH calibration TDS: ")
                        ec = int(inec)
                        ser[ecIndex].write(b'Cal,high' + str.encode(str(ec)) + b'\r')
                        if (checkok(ser[ecIndex]) == True):
                            print("Calibration successful.")
                            sleep(2)



