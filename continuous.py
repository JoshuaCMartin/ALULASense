import serial
import time
from time import sleep
import datetime
from pathlib import Path
import http.server
from http.server import HTTPServer
import threading
import socket

#Determine local IP address-----------------------------------------------------------------
s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
s.connect(("8.8.8.8", 80))
ip=str(s.getsockname()[0])
s.close()

#initialize HTTP Server---------------------------------------------------------------------
Handler = http.server.SimpleHTTPRequestHandler
serverPort = 8080
def startWebServer():

    httpd = HTTPServer(("0.0.0.0", serverPort), Handler)
    httpd.serve_forever()




def webServeWrapper():
    while True:
        try:
            startWebServer()
        except BaseException as e:
            print('{!r}; restarting thread'.format(e))
        else:
            print('exited normally, bad thread; restarting')


serverThread = threading.Thread(target=webServeWrapper)
try:
    serverThread.start()
except:
    print("Server Failed to Start")


# initialize sensors----------------------------------------------------------------------
ser = []
port = 0
portcheck = False
while(port < 99):
    addr = '/dev/ttyUSB' + str(port)
    serfile = Path(addr)
    if serfile.exists():
        print(addr + " is present.")
        portcheck = False
        try:
            ser.append(serial.Serial(addr, baudrate=9600, parity=serial.PARITY_NONE, bytesize=serial.EIGHTBITS, stopbits=serial.STOPBITS_ONE))
        except OSError as e:
            print (addr + " is INVALID")
        else:
            print(addr + " is valid")
    else:
        if (portcheck == False):
            print(addr + " is NOT present.")
        portcheck = True
    port = port + 1

#initialize reading variables-------------------------------------------------------------
numReadings = 5
sensorTypes = []
averages = [0.00 for x in range(len(ser))]
readings = [[0.00 for x in range(numReadings)]for y in range(len(ser))]


def sendreadcode(serx):
    serx.write(b'R\r')

def sendidcode(serx):
    ser[x].write(b'i\r')

def sendtemptrain(serx, temp):
    serx.write(b'T,' + str.encode(str(temp)) + b'\r')

def checkok(serx):
    message = ""
    raw = ""
    while raw != b'\r':
        raw = serx.read()
        message += raw.decode('utf-8')
    if ("OK" in message):
       return True
    else:
        print ("CHECKOK FAILED ", message)
        return False

def getidreading(serx):
    message = ""
    raw = ""
    while raw != b',':
        raw = serx.read()
    raw = ""
    while raw != b',':
        raw = serx.read()
        message += raw.decode('utf-8')
    message = message.rstrip(',')
    return message

def getreading(serx):
    message = ""
    raw = ""
    while raw != b'\r':
        raw = serx.read()
        message += raw.decode('utf-8')
    if ("OK" in message):
        message = ""
        raw = ""
        while raw != b'\r':
            raw = serx.read()
            message += raw.decode('utf-8')
    return message




#Tell sensors to shut up------------------------------------------------------------------
for x in range(len(ser)):
    ser[x].write(b'C,0\r')
    sleep(1)


#Retrieve Sensor Types---------------------------------------------------------------------
for x in range(len(ser)):
    sendidcode(ser[x])
    print("Querying Sensor ",  x)
    sensorTypes.append(getidreading(ser[x]))

#Configure EC sensors to output only EC----------------------------------------------------
for x in range(len(ser)):
    if(sensorTypes[x] == "EC"):
        getreading(ser[x])
        ser[x].write(b'O,EC,1\r')
        ser[x].write(b'O,TDS,0\r')
        ser[x].write(b'O,S,0\r')
        ser[x].write(b'O,SG,0\r')
        ser[x].write(b'K,1.0\r')
        checkok(ser[x])
        checkok(ser[x])

print(sensorTypes)
print("all done")



#Clear initial garbage readings-------------------------------------------------------------
for x in range(len(ser)):
    getreading(ser[x])
    checkok(ser[x])

#Check for temperature sensor--------------------------------------------------------------
try:
    rtdIndex = sensorTypes.index("RTD")
except ValueError:
    rtdIndex = -1

#get initial temp data for calibration------------------------------------------------------
if (rtdIndex > -1):
    for x in range(numReadings):
        sendreadcode(ser[rtdIndex])
        readings[rtdIndex][x] = round(float(getreading(ser[rtdIndex]).rstrip('\r')),2)
        checkok(ser[rtdIndex])
        sleep(1)

    print("initial temps ", readings[rtdIndex])
    averages[rtdIndex] = round(sum(readings[rtdIndex]) / len(readings[rtdIndex]),2)
    print("Average: ", averages[rtdIndex])


#initialize time delta----------------------------------------------------------------------
time0 = time.time()
time1 = time.time()
timedelta = 10

#Main loop---------------------------------------------------------------------------------
while True:
#TEMPERATURE CALIBRATION--------------------------------------------------------------
    for x in range(len(ser)):
        #if (sensorTypes[x] == "RTD"):

        if (sensorTypes[x] != "RTD"):
            if (rtdIndex > -1):
                sendtemptrain(ser[x], averages[rtdIndex])
                checkok(ser[x])

#ACQUIRE READINGS---------------------------------------------------------------------
    for y in range(numReadings):
        for x in range(len(ser)):
            sendreadcode(ser[x])
            readings[x][y] = round(float(getreading(ser[x]).rstrip('\r')),2)
            checkok(ser[x])

#CONSOLE OUTPUT-----------------------------------------------------------------
    print()

    print(datetime.datetime.now().replace(microsecond=0))
    print("IP address is: "+ str(ip) + ":"+ str(serverPort))

    for x in range(len(ser)):
        averages[x] = round(sum(readings[x]) / len(readings[x]), 2)

        if (sensorTypes[x] == "RTD"):
            print(str(sensorTypes[x]), ":", averages[x], "C,", round(float(((averages[x]*1.8)+32)),2), "F")
        else:
            print(str(sensorTypes[x]), ":", averages[x])

#LOGFILE OUTPUT-----------------------------------------------------------------
    logfname = str(datetime.date.today()) + ".html"
    log = open(logfname, "a")
    log.write("<table style=\"width:100%\">")
    log.write("<tr><th>")
    log.write(str(datetime.datetime.now().replace(microsecond=0)))
    log.write("</th><th>Average</th><th>Readings</th></tr>")

    for x in range(len(ser)):
        log.write("<tr><th>")
        log.write(str(sensorTypes[x]))
        log.write("</th>")
        log.write("<th>")
        log.write(str(averages[x]))
        log.write("</th>")
        log.write("<th>")
        log.write(str(readings[x]))
        log.write("</th>")
        log.write("</tr>")

    log.write("</table>")
    log.close()

#INDEX PAGE OUTPUT-----------------------------------------------------------------
    index = open("index.html", "w")
    index.write("<html>")
    index.write("<head><meta http-equiv=\"refresh\" content=\"")
    index.write(str(timedelta))
    index.write("\"></head>")
    index.write("<body>")
    index.write("<table style=\"width:100%; font-size: 8vw;\">")
    index.write("<tr><th>")
    index.write(str(datetime.datetime.now().replace(microsecond=0)))
    index.write("</th></tr><tr><th>")
    index.write("<a href=\"")
    index.write(logfname)
    index.write("\">Log File Here</a>")
    index.write("</th></tr>")
    for x in range(len(ser)):
        index.write("<tr><th>")
        index.write(str(sensorTypes[x]))
        index.write(": ")
        index.write(str(averages[x]))
        if (sensorTypes[x] == "RTD"):
            index.write("c, ")
            index.write(str(round(float(((averages[x]*1.8)+32)),2)))
            index.write("F")
        index.write("</th></tr>")
    index.write("</table></body></html>")
    index.close()
    print()

#TIMEDELTA CALCULATION FOR REFRESH-------------------------------------------------
    time0 = time1
    time1 = time.time()
    timedelta = time1-time0




